public class SimpleWar {
	
	public static void main(String[] args) {
		
		Deck deck = new Deck();
		deck.shuffle();
		System.out.println(deck);
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		
		// Main functionality of the game in this loop
		while(deck.length() > 0) {
			
			System.out.println("\nCards Drawn:");
			
			System.out.println("Player One: ");
			Card card1 = deck.drawTopCard();
			System.out.println(card1);
			double card1Weight = card1.calculateScore();
			System.out.println("Weight: " + card1Weight + "\n");
			
			System.out.println("Player Two: ");
			Card card2 = deck.drawTopCard();
			System.out.println(card2);
			double card2Weight = card2.calculateScore();
			System.out.println("Weight: " + card2Weight);
			
			System.out.println("\nPoints before:");
			
			System.out.println("Player One: " + playerOnePoints);
			System.out.println("Player Two: " + playerTwoPoints);
			
			if(card1.calculateScore() > card2.calculateScore()) {
				
				System.out.println("\nPlayer One Wins!");
				playerOnePoints++;
				
			}
			else {
				
				System.out.println("\nPlayer Two Wins!");
				playerTwoPoints++;
				
			}
			
			System.out.println("\nPoints After:");
			
			System.out.println("Player One: " + playerOnePoints);
			System.out.println("Player Two: " + playerTwoPoints);
			
			System.out.println("\n********************************************************");
			
		}
		
		// Congratulations message
		
		if(playerOnePoints > playerTwoPoints) {
			
			System.out.println("Congratulations Player 1, You Won the Whole Game!");
			
		}
		else if(playerTwoPoints > playerOnePoints) {
			
			System.out.println("Congratulations Player 2, You Won the Whole Game!");
			
		}
		
		else {
			
			System.out.println("There is no Winner, It was a Tie!");
			
		}
		
	}
	
}