import java.util.Random;

public class Deck {
	
	private Card[] cards; 
	private int numberOfCards;
	private Random rng;
	
	// Initializes the deck and gives all the cards a value and a suit
	public Deck() {
		
		numberOfCards = 52;
		cards = new Card[numberOfCards];
		rng = new Random();
		
		int index = 0;
		
		for(Suit suit : Suit.values()) {
			
			for(Rank rank : Rank.values()) {
				
				cards[index] = new Card(suit, rank);
				index++;
				
			}
			
		}
		
	}
	
	// Returns the number of cards left available
	public int length() {
		
		return this.numberOfCards;
		
	}
	
	// Returns the card at the top (last position) of the deck array
	public Card drawTopCard() {
		
		Card lastCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return lastCard;
		
	}
	
	// Returns all the cards with their values and suits
	public String toString() {
		
		String cardString = "";
		
		for(int i = 0; i < this.numberOfCards; i++) {
			cardString += this.cards[i] + "\n";
		}
		
		return cardString;
		
	}
	
	// Randomizes the position of cards in the deck
	public void shuffle() {
		
		for(int i = 0; i < this.numberOfCards; i++) {
			
			int randPos = rng.nextInt(this.numberOfCards - i) + i;
			Card cardToSwap = this.cards[i];
			this.cards[i] = this.cards[randPos];
			this.cards[randPos] = cardToSwap;
			
		}
		
	}
	
}