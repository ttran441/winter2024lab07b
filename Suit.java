public enum Suit {
	
	HEARTS(0.4),
	SPADES(0.3),
	DIAMONDS(0.2),
	CLUBS(0.1);
	
	// Fields
	private final double value;
	
	// Constructor
	private Suit(double value) {
		
		this.value = value;
		
	}
	
	// Getter for value
	public double getValue() {
		
		return this.value;
		
	}
	
}