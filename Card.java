public class Card {
	
	private Suit suit;
	private Rank rank;
	
	// Constructor
	public Card(Suit suit, Rank rank) {
		
		this.suit = suit;
		this.rank = rank;
		
	}
	
	// Getters for the suit and value
	public Suit getSuit() {
		
		return this.suit;
		
	}
	
	public Rank getRank() {
		
		return this.rank;
		
	}
	
	// Returns the value and the suit
	public String toString() {
		
		return this.rank + " of " + this.suit;
		
	}
	
	// Calculates the score based on the suit and value
	public double calculateScore() {
		
		return this.rank.getValue() + this.suit.getValue();
		
	}
	
}