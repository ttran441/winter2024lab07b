public enum Rank {
	
	ACE(1.0),
	TWO(2.0),
	THREE(3.0),
	FOUR(4.0),
	FIVE(5.0),
	SIX(6.0),
	SEVEN(7.0),
	EIGHT(8.0),
	NINE(9.0),
	TEN(10.0),
	JACK(11.0),
	QUEEN(12.0),
	KING(13.0);
	
	// Fields
	private final double value;
	
	// Constructor
	private Rank(double value) {
		
		this.value = value;
		
	}
	
	// Getter for value
	public double getValue() {
		
		return this.value;
		
	}
	
}